<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait Modifier {

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $user = Auth::user();
            if ($user) {
                $model->created_by = $user->name;
                $model->updated_by = $user->name;
            }
        });

        static::updating(function ($model) {
            $user = Auth::user();
            if ($user) {
                $model->updated_by = $user->name;
            }
        });
    }

}
