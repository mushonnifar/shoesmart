<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class Product extends Model
{
    use Modifier;
    protected $fillable = [
        "name",
        "brand_id",
        "sku",
        "price",
        "description",
        "is_displayed",
        "start_promo",
        "end_promo",
        "promo_price",
        "gender",
        "material_upper",
        "material_outer_sole",
        "care_label",
        "measurement",
        "final_price"
    ];

    public function brand(){
        return $this->belongsTo("App\Brand");
    }
}
