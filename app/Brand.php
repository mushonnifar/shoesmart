<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class Brand extends Model
{
    use Modifier;
    protected $fillable = ["name", "icon"];

    public function products(){
        return $this->hasMany("App\Product");
    }
}
