<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Modifier;

class Color extends Model
{
    use Modifier;
    protected $fillable = ["name", "rgb"];
}
