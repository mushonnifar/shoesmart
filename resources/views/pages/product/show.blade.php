@extends('layouts.default')

@section('title', 'Form Elements')

@push('css')
<script src='/assets/plugins/tinymce/tinymce.min.js' referrerpolicy="origin"></script>
@endpush

@section('content')

<!-- begin row -->
<div class="row">
    <!-- begin col-6 -->
    <div class="col-xl-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-stuff-10">
            <!-- begin panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title">Detail Product</h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
                @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
                @endif
                <form>
                    <fieldset>
                        @csrf
                        <input type="hidden" value="PUT" name="_method">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="name" value="{{$product->name}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Brand</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="name" value="{{$product->brand->name}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>SKU</label>
                            <input type="text" name="sku" class="form-control" id="sku" placeholder="sku" value="{{$product->sku}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" name="price" class="form-control" id="price" value="{{$product->price}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea id="description" name="description" rows="20" disabled>{{$product->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Display</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="is_displayed" id="is_displayed" value="1" disabled {{$product->is_displayed == '1' ? 'checked' : ''}}>
                                <label class="form-check-label" for="defaultRadio"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Start Promo</label>
                            <input type="date" name="start_promo" class="form-control" id="start_promo" value="{{$product->start_promo}}" disabled/>
                        </div>
                        <div class="form-group">
                            <label>End Promo</label>
                            <input type="date" name="end_promo" class="form-control" id="end_promo" value="{{$product->end_promo}}" disabled/>
                        </div>
                        <div class="form-group">
                            <label>Promo Price</label>
                            <input type="number" name="promo_price" class="form-control" id="promo_price" value="{{$product->promo_price}}" disabled/>
                        </div>
                        <div class="form-group">
                            <label>Gender</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="gender" value="Men" disabled {{$product->gender == 'Men' ? 'checked' : ''}}>
                                <label class="form-check-label" for="defaultRadio">Men</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="gender" value="Women" disabled {{$product->gender == 'Women' ? 'checked' : ''}}>
                                <label class="form-check-label" for="defaultRadio">Women</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Material Upper</label>
                            <input type="text" name="material_upper" class="form-control" id="material_upper" value="{{$product->material_upper}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Material Outer Sole</label>
                            <input type="text" name="material_outer_sole" class="form-control" id="material_outer_sole" value="{{$product->material_outer_sole}}" disabled />
                        </div>
                        <div class="form-group">
                            <label>Care Label</label>
                            <textarea id="care_label" name="care_label" rows="20">{{$product->care_label}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Measurement</label>
                            <textarea id="measurement" name="measurement" rows="20">{{$product->measurement}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Final Price</label>
                            <input type="number" name="final_price" class="form-control" id="final_price" value="{{$product->final_price}}" disabled />
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- end panel-body -->
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
@endsection

@push('scripts')
<script src="/assets/plugins/highlight.js/highlight.min.js"></script>
<script src="/assets/js/demo/render.highlight.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '#description',
        readonly : 1
    });
    tinymce.init({
        selector: '#care_label',
        readonly : 1
    });
    tinymce.init({
        selector: '#measurement',
        readonly : 1
    });
</script>
@endpush