@extends('layouts.default')

@section('title', 'Managed Tables')

@push('css')
<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="/assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')

<!-- begin panel -->
<div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading">
        <h4 class="panel-title">Category</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
        <a href="{{route('category.create')}}" type="button" class="btn btn-primary">Create</a>
        <table id="data-table" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th width="1%"></th>
                    <th class="text-nowrap">Name</th>
                    <th class="text-nowrap"></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- end panel-body -->
</div>
<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script>
    var datatable;
    $(document).ready(function() {
        datatable = $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{route('category.index')}}",
            columns: [{
                data: 'id',
                render: function(data, type, row, attr) {
                    return attr.row + attr.settings._iDisplayStart + 1;
                },
                searchable: false,
                orderable: false
            }, {
                data: 'name'
            }, {
                data: null,
                searchable: false,
                orderable: false,
                render: function(r, v, i) {
                    return `
                                <a href="{{route('category.index')}}/${r.id}/edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>&nbsp;
                                <form class="d-inline" action="{{route('category.index')}}/${r.id}" method="POST" onsubmit="return confirm('Move data to trash?')">
                                    @csrf 
                                    <input type="hidden" value="DELETE" name="_method">
                                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                </form>
                        `;
                }
            }]
        });
    });
</script>
@endpush